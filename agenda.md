title:agenda
language:en


Workshops to come :

### Through languages and rhythms
* command line, hpgl language digging
* halftones, patterns - generative patterns
* algorythmic drawings with vsketch or chiplotle?

### Through gestures
* single line fonts
* illustrations, interview on vector aesthetics - using vectors to draw, rather than bitmaps - conversations on how vectors influence aesthetics of drawings, historical perspective.

### Through sound
* sound

### To the multiple
* from plotter to riso - publications process

### Through tools
* adaptor printing
* Inkscape export plugin improve tooling?

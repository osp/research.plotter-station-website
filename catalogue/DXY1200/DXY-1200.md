Name: Roland DXY-1200
Year of introduction:
Machine dimensions: 613(W)x108(H)x417(D)mm
Weight: 5.7 kg 
Maximum plotting area: X 431.8mm Y 297mm
Maximum plotting speed: 420mm/sec
Paper size: max 420x297mm(ISO A3) or 432x279mm (ANSI B size)
Paper holding: Electrostatic paper holder
Distance accuracy: Whichever the greater value of ±0.3% or ±0.1mm of moving distance
Repeat accuracy: With same pen ±0.1mm or less, with different pen ±0.3mm or less
Right angle accuracy: 1mm/297mm
Display LED: POWER/ERROR, PAUSE, PAPER HOLD
Data buffer: 5k byte
Interface: Parallel (Centronics), Serial(RS-232C)
Number of pens: 8
Maximum pen height:
Pen force: 
Machine operating style: flatbed or moving medium. (fixed paper, fixed y-axis)
Multipages: no
State:
entry doors:
Related documents: Roland DXY-1300-1200-1100 Command Reference Manual - AF.pdf
Pictures:
---


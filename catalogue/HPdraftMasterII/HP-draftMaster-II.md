Name: HP DraftMaster II
Year of introduction: 1987
Machine dimensions: 1200mm(H) x 1346mm(W) x 508mm(D)
Weight: 73kg
Max. plotting area: ???
Max. plotting speed: 60.0 cm/s
Max. Paper size: 36 x 48in. (Architectural E) / A0
Paper holding: Rollers
Distance accuracy: 0.09% of the move or 0.25mm, whichever is greater.
Repeat accuracy: 0.1mm
Right angle accuracy: ???
Display LED: ???
Buffer Size: 25K (configurable)
Interface: ???
Number of pens: 8
Maximum pen height: ???
Pen force: 15-66 grams
Machine operating style: Paper moves in Y, pen in X
Multipages: No
State:
entry doors:
Related documents:[
  DraftMasterIandII_TechnicalData_5957-3783_4pages_Feb89.pdf,
  DraftMasterPlotter-UsersGuide-07595-90002-220pages-Feb88.pdf
]
Pictures:
---

title:intro
language:en


The plotter station is a project that provides us with a context to develop artistic research and an infrastructure to welcome others into our practice by organising public events like workshops and residencies, sharing knowledge and giving access to a collection of tools, both hard- and software.


A pen plotter is a mechanical drawing machine using 'normal' pens, they were originally developed in the 70's and 80's, used at the time for large format prints of architectural plans, CAD drawings or business graphs. They are controlled by a computer through HPGL, a seductively simple language prescribing the movement of the pens. Though contemporary cutting plotters are based on pen plotters, their slowness, cost of operation and large format inkjet printers made plotters irrelevant. But it is exactly the slowness, and performative movement that makes them empathic, while their simplicity allows for easy appropriation, their unoptimized, repetitive movement gives insight into the commands it recieves from the computer. As a plotter draw with a pen, it draws lines rather than filled shapes, the shape of these lines are influenced by the pen; its thichness, angle and ink. Therefor, plotters are great companions for our research.


Our proposal is driven by a collective belief that there is value in the specific expertise and experience present in OSP. The intention is to expand it through technical and artistic practice based research, on our own, or by solicitating a network of experts, as well as to make it available, and disseminate it to other visual practitioners in Brussels, Belgium and abroad.

These two intentions intertwine, and reinforce each other, visualized in the following graph.

![](https://cloud.osp.kitchen/s/tAQZAtDsabqwZ3W/preview "Schéma de la station de traçage comme œuvre de recherche, généré avec Graphviz, 2023")

Making graphic design with "a professional graphic worklow using only open-source tools" in our practice quickly meant bending existing (software-)tools and making new ones. For us toolmaking is as much an artistic gesture and part of our practice as the works produced with them. This intimate relationship with our tools is an attempt to break away from the hegemony of standardized proprietary software, taking into account the fact that there is not just one way of doing things, and that our tools themselves already reflect graphic and cultural forms.
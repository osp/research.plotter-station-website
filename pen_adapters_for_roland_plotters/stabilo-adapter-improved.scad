height = 32;
thin_top = 10;
inner_radius = (9.1 / 2); // 8.3 with some space
outer_radius = (11.5 / 2);

handle_radius = (16.5 / 2);
handle_thickness_outer = 1.3;
handle_thickness_inner = 1.9;
handle_position = 12;

facets = 240;

difference() {
    union() {
        // Base cylinder
        cylinder(h=height-thin_top, r=outer_radius, $fn=facets);
        translate([0,0,height-thin_top])
            cylinder(h=thin_top, r=inner_radius+.5, $fn=6);
        
        // Handle, base
        translate([0, 0, handle_position])
            cylinder(h=handle_thickness_outer, r=handle_radius, $fn=facets);
        
        // Handle, angled top
        translate([0, 0, handle_position + handle_thickness_outer])
            cylinder(h=handle_thickness_inner - handle_thickness_outer, r1=handle_radius, r2=outer_radius, $fn=facets);
    }
    union() {
        cylinder(h=2, r1=inner_radius+.5, r2=inner_radius, $fn=6);
        translate([0,0,2]) cylinder(h=height-2, r=inner_radius, $fn=6);
    }
}

//support
difference() {
    cylinder(h=handle_position-.2,r=handle_radius, $fn=facets);
    cylinder(h=handle_position-.2,r=handle_radius - 0.2, $fn=facets);
}

difference() {
    cylinder(h=handle_position-.2,r=handle_radius - 1.2, $fn=facets);
    cylinder(h=handle_position-.2,r=handle_radius - 1.4, $fn=facets);
}

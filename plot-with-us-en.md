title:plott with us
language:en

# Contact

The plotter station is a space to welcome others into the practice through public events like workshops and residencies. See the [agenda section](agenda.md)
But don't hesitate to contact us if you have a question, an educational project or a research project linked to the station. We can then arrange a specific appointment.
plotwithus@osp.kitchen

To prepare for your visit, here's some practical information.

The plotter station is hosted by OSP.
OSP workspace is part of [Meyboom artist-run-spaces](https://meyboom.space/). 
Meyboom brings together a community of individuals and collectives working in different fields that has continually evolved since its formation in the year 2013.

Boulevard Pachécolaan 34-2, 1000 Brussels 

The workspace is located on the 2nd floor of a building equipped with elevators, one of the access doors has a ramp. The floor has wheelchair accessible bathrooms without handrails. Public transport is a few hundred meters away (Botanique/Kruidtuin, Rogier, Central station, Brussels Congres). 
The building has bike racks behind a lockable door.

## Code of conduct


## FAQ

How can I use the plotter?
:  Either you 

Can I rent a machine?
:  You can use the plotter in the plotter station and pay for the time you'll be using the machines. 
